all:
	git clone https://github.com/hakimel/reveal.js.git --branch 2.4.0
	git clone https://github.com/regisb/reveal.js-fullscreen-img.git

clean:
	rm -rf reveal.js
	rm -rf reveal.js-fullscreen-img
